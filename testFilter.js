function filter(elements,cb)
{
    let acceptable=new Array()
    i=0;
    for (i=0;i<elements.length;i++)
    {
        if (cb(elements[i]))
        {
            acceptable.push(elements[i])
        }
    } 
    return acceptable
}
const items = [1, 2, 3, 4, 5, 5];
console.log(filter(items,function(num){ return num % 2 == 0; }))