const nestedArray = [1, [2], [[3]], [[[4]]]];
//console.log(nestedArray[0])
function flatten(elements,result=[])
{   
    i=0;
    for (i=0;i<elements.length;i++)
    {
        
        if (Array.isArray(elements[i]))
        {
            //console.log( elements[i])
            flatten(elements[i],result)
        }
        else
        {
            //console.log( elements[i])
            result.push(elements[i])
        }
        
    } 
    //console.log(result)
    return result
}
console.log(flatten(nestedArray))